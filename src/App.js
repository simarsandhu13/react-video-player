import "./App.css";
import React, { useState } from "react";
import ReactPlayer from "react-player";
import S3FileUpload from "react-s3";

// Username: simar_viralnation
// Access Key: AKIAXICOLQGEGBWYLP6K
// Secret Ket: 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
// Bucket name: vn-academy-upload

const config = {
  bucketName: "vn-academy-upload",
  dirName: "simar_viralnation" /* optional */,
  region: "ca-central-1",
  accessKeyId: "AKIAXICOLQGEGBWYLP6K",
  secretAccessKey: "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",
};

function App() {
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [selectedFile, setSelectedFile] = useState();

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const handleSubmission = () => {
    console.log(selectedFile);
    S3FileUpload.uploadFile(selectedFile, config)
      .then((data) => console.log(data))
      .catch((err) => console.error(err));
  };

  return (
    <div className="container pb-5">
      <div className="row p-3">
        <div className="col-12 text-center">
          <h1>Video Player</h1>
        </div>
      </div>
      <div className="row justify-content-center p-2">
        <div className="col-12 col-lg-6 p-3">
          <div className="player-wrapper">
            <ReactPlayer
              className="react-player"
              url="https://youtu.be/awqb75Q-Pn4"
              width="100%"
              height="100%"
              controls={true}
            />
          </div>
        </div>
        <div className="col-12 col-lg-6 p-3">
          <div className="player-wrapper">
            <ReactPlayer
              className="react-player"
              url="https://youtu.be/olg_DXRvwOI"
              width="100%"
              height="100%"
              controls={true}
            />
          </div>
        </div>
        <div className="col-12 col-lg-6 p-3">
          <div className="player-wrapper">
            <ReactPlayer
              className="react-player"
              url="https://youtu.be/eA8Pnngw240"
              width="100%"
              height="100%"
              controls={true}
            />
          </div>
        </div>
        <div className="col-12 col-lg-6 p-3">
          <div className="player-wrapper">
            <ReactPlayer
              className="react-player"
              url="https://vn-academy-dev-poc-vod.s3.us-east-2.amazonaws.com/Top+Tips+for+Superstar+Presentations+by+Todd+Reubold.mp4"
              width="100%"
              height="100%"
              controls={true}
            />
          </div>
        </div>
      </div>
      <div className="row justify-content-center p-3">
        <div className="col-12 text-center pb-2">
          <h3>Upload New Video</h3>
        </div>
        <input
          className="form-control"
          style={{ width: "300px" }}
          type="file"
          onChange={changeHandler}
        />
        <div className="col-12 text-center pt-3">
          <button
            type="button"
            className="btn btn-outline-dark"
            onClick={handleSubmission}
            disabled={!isFilePicked}
          >
            Upload Video
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
